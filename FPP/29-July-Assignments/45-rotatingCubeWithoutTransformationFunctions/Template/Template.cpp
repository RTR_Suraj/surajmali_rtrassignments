#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define PI 3.1415926535898
#define USE_MATH_DEFINES 1

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLfloat angleCubeDegrees = 0.0f;
GLfloat angleCubeRadians = 0.0f;

// array declarations(fixed)
GLfloat identityMatrix[16];
GLfloat translationMatrix[16];
GLfloat scaleMatrix[16];

// these are changing arrays
GLfloat rotateXMatrix[16];
GLfloat rotateYMatrix[16];
GLfloat rotateZMatrix[16];

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display();
	void update();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline Using Native Windowing : Point at"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
				display();
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	void fillIdentityMatrix();
	void fillTranslationMatrix();
	void fillScaleMatrix();

	fillIdentityMatrix();
	fillTranslationMatrix();
	fillScaleMatrix();

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	// declarations
	void drawCube();
	void fillrotateXMatrix();
	void fillrotateYMatrix();
	void fillrotateZMatrix();

	// filling of arrays
	fillrotateXMatrix();
	fillrotateYMatrix();
	fillrotateZMatrix();


	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	glLoadMatrixf(identityMatrix);
	//glLoadIdentity();
	glMultMatrixf(translationMatrix);
	//glTranslatef(0.0f, 0.0f, -10.0f);
	glMultMatrixf(scaleMatrix);
	//glScalef
	
	glMultMatrixf(rotateXMatrix);
	glMultMatrixf(rotateYMatrix);
	glMultMatrixf(rotateZMatrix);
	//glRotatef(angleCubeDegrees, 1.0f, 1.0f, 1.0f);
	drawCube();

	SwapBuffers(ghdc);
	//glFlush();
}

void drawCube()
{
	glBegin(GL_QUADS);
	
	// front face
	glColor3f(1.0f,0.0f,0.0f); // red color

	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// right face
	glColor3f(0.0f, 1.0f, 0.0f); // green color

	glVertex3f(1.0f, 1.0f, -1.0f); // right top of right face
	glVertex3f(1.0f, 1.0f, 1.0f); // left top of right face
	glVertex3f(1.0f, -1.0f, 1.0f); // left bottom of right face
	glVertex3f(1.0f, -1.0f, -1.0f); // right bottom of right face

	// back face
	glColor3f(0.0f, 0.0f, 1.0f); // blue color

	glVertex3f(-1.0f, 1.0f, -1.0f); // right top of back face
	glVertex3f(1.0f, 1.0f, -1.0f); // left top of back face
	glVertex3f(1.0f, -1.0f, -1.0f); // left bottom of back face
	glVertex3f(-1.0f, -1.0f, -1.0f); // right bottom of back face

	// left face
	glColor3f(1.0f, 1.0f, 0.0f); // red color

	glVertex3f(-1.0f, 1.0f, 1.0f); // right top of left face
	glVertex3f(-1.0f, 1.0f, -1.0f); // left top of left face
	glVertex3f(-1.0f, -1.0f, -1.0f); // left bottom of left face
	glVertex3f(-1.0f, -1.0f, 1.0f); // right bottom of left face

	// top face
	glColor3f(1.0f, 0.0f, 1.0f); // green color

	glVertex3f(1.0f, 1.0f, -1.0f); // right top of top face
	glVertex3f(-1.0f, 1.0f, -1.0f); // left top of top face
	glVertex3f(-1.0f, 1.0f, 1.0f); // left bottom of top face
	glVertex3f(1.0f, 1.0f, 1.0f); // right bottom of top face

	// bottom face
	glColor3f(0.0f, 1.0f, 1.0f); // green color

	glVertex3f(1.0f, -1.0f, 1.0f); // right top of bottom face
	glVertex3f(-1.0f, -1.0f, 1.0f); // left top of bottom face
	glVertex3f(-1.0f, -1.0f, -1.0f); // left bottom of bottom face
	glVertex3f(1.0f, -1.0f, -1.0f); // right bottom of bottom face


	glEnd();
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | 
			SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void update()
{
	angleCubeRadians = angleCubeDegrees * (PI / 180.0f);
	angleCubeDegrees = angleCubeDegrees + 0.1f;
	
	if (angleCubeDegrees >= 360.0f)
	{
		angleCubeDegrees = 0.0f;
	}
}

/*
This is equivalent to glIdentity();
*/
void fillIdentityMatrix()
{
	identityMatrix[0] = 1.0f;
	identityMatrix[1] = 0.0f;
	identityMatrix[2] = 0.0f;
	identityMatrix[3] = 0.0f;

	identityMatrix[4] = 0.0f;
	identityMatrix[5] = 1.0f;
	identityMatrix[6] = 0.0f;
	identityMatrix[7] = 0.0f;

	identityMatrix[8] = 0.0f;
	identityMatrix[9] = 0.0f;
	identityMatrix[10] = 1.0f;
	identityMatrix[11] = 0.0f;

	identityMatrix[12] = 0.0f;
	identityMatrix[13] = 0.0f;
	identityMatrix[14] = 0.0f;
	identityMatrix[15] = 1.0f;
}

/*
This is equivalent to glTranslatef(0.0f,0.0f,-4.0f);
*/
void fillTranslationMatrix()
{
	translationMatrix[0] = 1.0f;
	translationMatrix[1] = 0.0f;
	translationMatrix[2] = 0.0f;
	translationMatrix[3] = 0.0f;

	translationMatrix[4] = 0.0f;
	translationMatrix[5] = 1.0f;
	translationMatrix[6] = 0.0f;
	translationMatrix[7] = 0.0f;

	translationMatrix[8] = 0.0f;
	translationMatrix[9] = 0.0f;
	translationMatrix[10] = 1.0f;
	translationMatrix[11] = 0.0f;

	translationMatrix[12] = 0.0f;
	translationMatrix[13] = 0.0f;
	translationMatrix[14] = -6.0f;
	translationMatrix[15] = 1.0f;
}

/*
This is equivalent to glScalef(0.75f,0.75f,0.75f);
*/
void fillScaleMatrix()
{
	scaleMatrix[0] = 0.75f; // scale to x axis(jadi kami/jast karne)
 	scaleMatrix[1] = 0.0f;
	scaleMatrix[2] = 0.0f;
	scaleMatrix[3] = 0.0f;

	scaleMatrix[4] = 0.0f;
	scaleMatrix[5] = 0.75f;// scale to y axis(Unchi kami/jast karne)
	scaleMatrix[6] = 0.0f;
	scaleMatrix[7] = 0.0f;

	scaleMatrix[8] = 0.0f;
	scaleMatrix[9] = 0.0f;
	scaleMatrix[10] = 0.75f; // scale to z axis(kholi kami/jast karne)
	scaleMatrix[11] = 0.0f;

	scaleMatrix[12] = 0.0f;
	scaleMatrix[13] = 0.0f;
	scaleMatrix[14] = 0.0f;
	scaleMatrix[15] = 1.0f;
}

/*
This is equivalent to glRotate around x axis 
*/
void fillrotateXMatrix()
{
	rotateXMatrix[0] = 1.0f; 
	rotateXMatrix[1] = 0.0f;
	rotateXMatrix[2] = 0.0f;
	rotateXMatrix[3] = 0.0f;

	rotateXMatrix[4] = 0.0f;
	rotateXMatrix[5] = cos(angleCubeRadians);
	rotateXMatrix[6] = sin(angleCubeRadians);
	rotateXMatrix[7] = 0.0f;

	rotateXMatrix[8] = 0.0f;
	rotateXMatrix[9] = -sin(angleCubeRadians);
	rotateXMatrix[10] = cos(angleCubeRadians);
	rotateXMatrix[11] = 0.0f;

	rotateXMatrix[12] = 0.0f;
	rotateXMatrix[13] = 0.0f;
	rotateXMatrix[14] = 0.0f;
	rotateXMatrix[15] = 1.0f;
}

/*
This is equivalent to glRotate around y axis
*/
void fillrotateYMatrix()
{
	rotateYMatrix[0] = cos(angleCubeRadians);
	rotateYMatrix[1] = 0.0f;
	rotateYMatrix[2] = -sin(angleCubeRadians);
	rotateYMatrix[3] = 0.0f;

	rotateYMatrix[4] = 0.0f;
	rotateYMatrix[5] = 1.0f;
	rotateYMatrix[6] = 0.0f;
	rotateYMatrix[7] = 0.0f;

	rotateYMatrix[8] = sin(angleCubeRadians);
	rotateYMatrix[9] = 0.0f;
	rotateYMatrix[10] = cos(angleCubeRadians);
	rotateYMatrix[11] = 0.0f;

	rotateYMatrix[12] = 0.0f;
	rotateYMatrix[13] = 0.0f;
	rotateYMatrix[14] = 0.0f;
	rotateYMatrix[15] = 1.0f;
}

/*
This is equivalent to glRotate around z axis
*/
void fillrotateZMatrix()
{
	rotateZMatrix[0] = cos(angleCubeRadians);
	rotateZMatrix[1] = sin(angleCubeRadians);
	rotateZMatrix[2] = 0.0f;
	rotateZMatrix[3] = 0.0f;

	rotateZMatrix[4] = -sin(angleCubeRadians);
	rotateZMatrix[5] = cos(angleCubeRadians);
	rotateZMatrix[6] = 0.0f;
	rotateZMatrix[7] = 0.0f;

	rotateZMatrix[8] = 0.0f;
	rotateZMatrix[9] = 0.0f;
	rotateZMatrix[10] = 1.0f;
	rotateZMatrix[11] = 0.0f;

	rotateZMatrix[12] = 0.0f;
	rotateZMatrix[13] = 0.0f;
	rotateZMatrix[14] = 0.0f;
	rotateZMatrix[15] = 1.0f;
}
