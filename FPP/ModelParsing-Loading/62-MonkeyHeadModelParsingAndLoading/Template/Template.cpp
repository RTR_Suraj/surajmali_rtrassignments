#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <vector>

#include <stdio.h>
#include <stdlib.h>

using namespace std;




#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;
bool gbIsLight = false;

GLfloat angleMonkeyHead = 0.0f;

// global arrays for lights effect
GLfloat light_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat light_diffused[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 1.0f,0.0f,0.0f,0.0f };

GLfloat material_shininess[] = { 50.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };

// symbolic constants
#define TRUE 1
#define FALSE 0

#define BUFFER_SIZE 256		// max lenght of string in mesh file
#define S_EQUAL		0		// return value of strcmp when strings are matched

#define WIN_WIDTH 800		// width of window
#define WIN_HEIGHT 600		// height of window

#define NR_POINT_COORDS	3		// max no of points
#define NR_TEXTURE_COORDS	2	// max no of texture cords
#define NR_NORMAL_COORDS	3	// max no of normal cords
#define NR_FACE_TOKENS	3		// min no of entries in face data

// vectore of vector of floats to hold vetices data
std::vector<std::vector<float>> g_vertices;

// vectore of vector of floats to hold texture data
std::vector<std::vector<float>> g_textures;

// vectore of vector of floats to hold normal data
std::vector<std::vector<float>> g_normals;

// vectore of vector of int to hold face data
// face data gives
std::vector<std::vector<int>> g_face_tri, g_face_texture, g_face_normals;


// handle to mesh file
FILE *g_fp_meshfile = NULL;

// handle to log file
FILE *g_fp_logfile = NULL;

// hold a line in a mesh file
char g_line[BUFFER_SIZE];



//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display();
	void update();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline Using Native Windowing : Rotating pyramid"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
				display();
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C:
			//MessageBox(hwnd, TEXT("Message"), TEXT("Message"), LB_OKAY);
			if (gbIsLight == false)
			{
				glEnable(GL_LIGHTING);
				gbIsLight = true;
			}
			else
			{
				glDisable(GL_LIGHTING);
				gbIsLight = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);
	void uninitialize();
	void loadMonkeyHeadMeshData();


	g_fp_logfile = fopen("Logger.log", "w");
	if (!g_fp_logfile)
	{
		uninitialize();
	}

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);


	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffused);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glEnable(GL_LIGHT0);

	// read the mesh file ,parse it and extract required vertices,textures,normals and face data
	loadMonkeyHeadMeshData();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void loadMonkeyHeadMeshData()
{
	void uninitialize();

	// open the mesh file
	g_fp_meshfile = fopen("MonkeyHead.OBJ", "r");

	if (!g_fp_meshfile)
	{
		MessageBox(ghwnd, TEXT("Error"), TEXT("Error loading file"), MB_OK);
		uninitialize();
	}

	// seperator strings - These will seperators from the mesh file such as space or / etc
	// string holding space char for strtok api
	char *sep_space = " ";

	// string holding forward slash char for strtok api
	char *sep_fslash = "/";

	// Token holders
	// char pointer to hold first word in a line
	char *first_token = NULL;

	// char pointer to hold next word in a line separted by space or slash
	char *token = NULL;

	// array of character pointer to hold face entries
	// face entries - 111/232/234 313/324/546 312/4646/676 - We hold these groups together as one array entry
	// 111(vertex line no of p1 point from obj file)
	// 232(texture line no of p1 point from obj file)
	// 234(normal line no of p1 point from obj file)
	char *face_tokens[NR_FACE_TOKENS];

	// no of tokens in above face entries vector
	int nr_tokens;

	// character pointer to hold string associated with 
	// vertes index
	char *token_vertex_index = NULL;

	// for texture index
	char *token_texture_index = NULL;

	// for normal index
	char *token_normal_index = NULL;

	// while there is line in the file
	while (fgets(g_line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{
		// find a seperator and get fisrt token of each new line
		first_token = strtok(g_line, sep_space);


		// check whether this first token is vertex data
		if (strcmp(first_token, "v") == S_EQUAL)
		{
			// create a vector to hold coorinates of float type 
			std::vector<float> vec_point_cords(NR_POINT_COORDS);

			// Do the following NR_POINT_CORDS times
			// Step 1 - Get the next token 
			// step 2 - feed this token to atof to get fload data
			// step 3 - add this data to vec_point_cords
			// End of loop
			// step 4 - Add vec_point_cords data to g_vertices - vec_point_cords contains 3 float nos of current line in read

			for (int i = 0; i != NR_POINT_COORDS; i++)
			{
				vec_point_cords[i] = atof(strtok(NULL, sep_space));
			}
			g_vertices.push_back(vec_point_cords);
		}
		// check whether this first token is texture data
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			// create a vector to hold texture coorinates of float type 
			std::vector<float> vec_texture_cords(NR_TEXTURE_COORDS);

			// Do the following NR_TEXTURE_CORDS times
			// Step 1 - Get the next token 
			// step 2 - feed this token to atof to get fload data
			// step 3 - add this data to vec_texture_cords
			// End of loop
			// step 4 - Add vec_texture_cords data to g_textures - vec_texture_cords contains 3 float nos of current line in read

			for (int i = 0; i < NR_TEXTURE_COORDS; i++)
			{
				vec_texture_cords[i] = atof(strtok(NULL, sep_space));
			}
			g_textures.push_back(vec_texture_cords);
		}
		// check whether this first token is normal data
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			// create a vector to hold noraml coorinates of float type 
			std::vector<float> vec_normal_cords(NR_NORMAL_COORDS);

			// Do the following NR_NORMAL_CORDS times
			// Step 1 - Get the next token 
			// step 2 - feed this token to atof to get fload data
			// step 3 - add this data to vec_texture_cords
			// End of loop
			// step 4 - Add vec_normal_cords data to g_normals - vec_normal_cords contains 3 float nos of current line in read

			for (int i = 0; i < NR_NORMAL_COORDS; i++)
			{
				vec_normal_cords[i] = atof(strtok(NULL, sep_space));
			}
			g_normals.push_back(vec_normal_cords);
		}
		// check whether this first token is face data
		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			// three vectors of int with lenght 3 to hold each slice of aray seperated by space i.e
			// triangles positional cords, triangles texture cords and triangles normal cords
			std::vector<int> triangle_vertex_indices(3), texture_vertex_indices(3), normal_vertex_indices(3);

			// Initialise all char in face tokens to NULL
			memset((void*)face_tokens, 0, NR_FACE_TOKENS);

			// extract 3 pieces of information from face_tokens line
			nr_tokens = 0;
			while (token = strtok(NULL, sep_space))
			{
				if (strlen(token)<3)
				{
					break;
				}
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}

			// each face entry data contains minimum of 3 fields
			// so constrct a traingle out of it with -
			// Step 1- traingle cord data
			// Step 2- texture cord data
			// Step 3- normal cord data
			// Step 4- put all this data in triangle_vertex_indices,texture_vertex_indices,normal_vertex_indices

			for (int i = 0; i != NR_FACE_TOKENS; ++i)
			{
				token_vertex_index = strtok(face_tokens[i], sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				token_normal_index = strtok(NULL, sep_fslash);

				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
				normal_vertex_indices[i] = atoi(token_normal_index);
			}

			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		}

		// Initialise line buffer to NULL
		memset((void*)g_line, (int)'\0', BUFFER_SIZE);
	}
	// close mesh file
	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// translate the monkey head
	glTranslatef(0.0f, 0.0f, -5.0f);

	// rotate monkeyhead
	glRotatef(angleMonkeyHead, 0.0f, 1.0f, 0.0f);

	// scale the monkey head
	glScalef(1.5f, 1.5f, 1.5f);

	// keep counter clock wise winding of vertices
	glFrontFace(GL_CCW);

	// set polygon mode
	if (gbIsLight)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	

	// step 1- for every face index in mesh file in g_face_tri do follwoing -
	// GL_TRIANGLES
	// first for loop will be eqaul to g_face_tri.size(), in inner for loop access each enaty from g_face_tri[i]
	// from this each entry retrive x,y and z of conrds

	for (int i = 0; i < g_face_tri.size(); ++i)
	{
		glBegin(GL_TRIANGLES);
		//glColor3f(1.0f, 0.0f, 0.0f);

		for (int j = 0; j != g_face_normals[i].size(); j++)
		{
			int ni = g_face_normals[i][j] - 1;
			glNormal3f(g_normals[ni][0], g_normals[ni][1], g_normals[ni][2]);
		}

		fprintf(g_fp_logfile, "Size 1 %d\n", g_face_tri[i].size());
		for (int j = 0; j != g_face_tri[i].size(); j++)
		{
			int vi = g_face_tri[i][j] - 1;
			glVertex3f(g_vertices[vi][0], g_vertices[vi][1], g_vertices[vi][2]);
		}

		

		glEnd();
	}
	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void update()
{
	angleMonkeyHead = angleMonkeyHead + 1.0f;
	if (angleMonkeyHead >= 360.0f)
	{
		angleMonkeyHead = 0.0f;
	}
}
