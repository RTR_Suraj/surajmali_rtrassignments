#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define PI 3.1415926535898

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

bool gbIsLight = false;
GLUquadric *quadric;

GLfloat anglePyramid = 0.0f;

// global arrays for lights effect

// Lights of RED color
GLfloat red_lights_0_ambient[] = {0.0f,0.0f,0.0f,0.0f};
GLfloat red_lights_0_diffuse[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat red_lights_0_specular[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat red_lights_0_shininess[] = { 50.0f,50.0f,50.0f,50.0f };
GLfloat red_lights_0_position[] = { 0.0f,0.0f,0.0f,0.0f };

// Lights of GREEN color
GLfloat green_lights_1_ambient[] = {0.0f,0.0f,0.0f,0.0f};
GLfloat green_lights_1_diffuse[] = {0.0f,1.0f,0.0f,1.0f};
GLfloat green_lights_1_specular[] = {0.0f,1.0f,0.0f,1.0f};
GLfloat green_lights_1_shininess[] = {50.0f,50.0f,50.0f,50.0f};
GLfloat green_lights_1_position[] = {0.0f,0.0f,0.0f,0.0f};

// Lights of BLUE color
GLfloat blue_lights_2_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat blue_lights_2_diffuse[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat blue_lights_2_specular[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat blue_lights_2_shininess[] = { 50.0f,50.0f,50.0f,50.0f };
GLfloat blue_lights_2_position[] = { 0.0f,0.0f,0.0f,0.0f };

// material properties
GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffused[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess[] = { 50.0f,50.0f,50.0f,50.0f };

// variables for lights animation
GLfloat gRedLightAngle = 0.0f;
GLfloat gGreenLightAngle = 0.0f;
GLfloat gBlueLightAngle = 0.0f;


//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display();
	void update();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline Using Native Windowing : gluLookAt"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
				display();
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C: // l button handled
			//MessageBox(hwnd, TEXT("Message"), TEXT("Message"), LB_OKAY);
			if (gbIsLight == false)
			{
				glEnable(GL_LIGHTING);
				gbIsLight = true;
			}
			else
			{
				glDisable(GL_LIGHTING);
				gbIsLight = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | 
			SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// changes for lighting
	// red light 
	/*
	Here we are not defining position of lights, we are givinig it runtime in display so it will be like lights animation
	*/
	glLightfv(GL_LIGHT0, GL_AMBIENT, red_lights_0_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, red_lights_0_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, red_lights_0_specular);
	glLightfv(GL_LIGHT0, GL_SHININESS, red_lights_0_shininess);

	// green light
	glLightfv(GL_LIGHT1, GL_AMBIENT, green_lights_1_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, green_lights_1_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, green_lights_1_specular);
	glLightfv(GL_LIGHT1, GL_SHININESS, green_lights_1_shininess);

	// blue light
	glLightfv(GL_LIGHT2, GL_AMBIENT, blue_lights_2_ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, blue_lights_2_diffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, blue_lights_2_specular);
	glLightfv(GL_LIGHT2, GL_SHININESS, blue_lights_2_shininess);

	// material of sphere
	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambient);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	void drawSpehre();

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
		gluLookAt(0.0f, 0.0f, 0.1f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);


	glPushMatrix();
		glRotatef(gRedLightAngle, 1.0f, 0.0f, 0.0f);
		red_lights_0_position[1] = gRedLightAngle;
		glLightfv(GL_LIGHT0, GL_POSITION, red_lights_0_position);
	glPopMatrix();

	glPushMatrix();
	//gluLookAt(0.0f, 0.0f, 0.1f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
		glRotatef(gGreenLightAngle, 0.0f, 1.0f, 0.0f);
		green_lights_1_position[0] = gGreenLightAngle;
		glLightfv(GL_LIGHT1, GL_POSITION, green_lights_1_position);
	glPopMatrix();

	glPushMatrix();
	//gluLookAt(0.0f, 0.0f, 0.1f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
		glRotatef(gBlueLightAngle, 0.0f, 1.0f, 0.0f);
		blue_lights_2_position[0] = gBlueLightAngle;
		glLightfv(GL_LIGHT2, GL_POSITION, blue_lights_2_position);
	glPopMatrix();


	glTranslatef(0.0f, 0.0f, -2.0f);
	drawSpehre();

	glPopMatrix();

	glPopMatrix();

	SwapBuffers(ghdc);
}

void drawSpehre()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	//glColor3f(1.0f, 1.0f, 0.0f);

	gluSphere(quadric, 0.75f, 30, 30); // sun from solar system
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void update()
{
	gRedLightAngle = gRedLightAngle + 0.03f;
	if (gRedLightAngle >= 360.0f)
	{
		gRedLightAngle = 0.0f;
	}

	gGreenLightAngle = gGreenLightAngle + 0.01f;
	if (gGreenLightAngle >= 360.0f)
	{
		gGreenLightAngle = 0.0f;
	}

	gBlueLightAngle = gBlueLightAngle + 0.03f;
	if (gBlueLightAngle >= 360.0f)
	{
		gBlueLightAngle = 0.0f;
	}
}
