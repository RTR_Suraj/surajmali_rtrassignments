#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define PI 3.1415926535898

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

bool gbIsLight = false;
GLUquadric *quadric;

GLfloat gLightAngle = 0.0f;
GLfloat alpha = 1.0f;

GLboolean gbIsXKeyPressed = GL_FALSE;
GLboolean gbIsYKeyPressed = GL_FALSE;
GLboolean gbIsZKeyPressed = GL_FALSE;

GLfloat light_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat light_diffused[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 0.0f,0.0f,0.0f,1.0f };

// materials array

// 1st column che

// 1 sphere 1 column - emerald
GLfloat sphere_1_1_material_ambient[] = { 0.0215f,0.1745f,0.0215f,alpha };
GLfloat sphere_1_1_material_diffuse[] = { 0.7568f,0.61424f,0.07568f,alpha };
GLfloat sphere_1_1_material_specular[] = { 0.633f,0.727811f,0.633f,alpha };
GLfloat sphere_1_1_material_shininess = 0.6 * 128;

// ***** 2nd sphere on 1st column, jade *****
GLfloat sphere_1_2_material_ambient[] = { 0.135f,0.2225f,0.1575f,alpha };
GLfloat sphere_1_2_material_diffuse[] = { 0.54f,0.89f,0.63f,alpha };
GLfloat sphere_1_2_material_specular[] = { 0.316228f,0.316228f,0.316228f,alpha };
GLfloat sphere_1_2_material_shininess = 0.1 * 128;

// ***** 3rd sphere on 1st column, obsidian *****
GLfloat sphere_1_3_material_ambient[] = { 0.05375f,0.05f,0.06625f,alpha };
GLfloat sphere_1_3_material_diffuse[] = { 0.18275f,0.17f,0.22525f,alpha };
GLfloat sphere_1_3_material_specular[] = { 0.332741f,0.328634f,0.346435f,alpha };
GLfloat sphere_1_3_material_shininess = 0.3 * 128;

// ***** 4th sphere on 1st column, pearl *****
GLfloat sphere_1_4_material_ambient[] = { 0.25f,0.20725f,0.20725f,alpha };
GLfloat sphere_1_4_material_diffuse[] = { 1.0f,0.829f,0.829f,alpha };
GLfloat sphere_1_4_material_specular[] = { 0.296648f,0.296648f,0.296648f,alpha };
GLfloat sphere_1_4_material_shininess = 0.088 * 128;

// ***** 5th sphere on 1st column, ruby *****
GLfloat sphere_1_5_material_ambient[] = { 0.1745f,0.01175f,0.01175f,alpha };
GLfloat sphere_1_5_material_diffuse[] = { 0.61424f,0.04136f,0.04136f,alpha };
GLfloat sphere_1_5_material_specular[] = { 0.7278118f,0.626959f,0.626959f,alpha };
GLfloat sphere_1_5_material_shininess = 0.6 * 128;

// ***** 6th sphere on 1st column, turquoise *****
GLfloat sphere_1_6_material_ambient[] = { 0.1f,0.18725f,0.1745f,alpha };
GLfloat sphere_1_6_material_diffuse[] = { 0.396f,0.74151f,0.69102f,alpha };
GLfloat sphere_1_6_material_specular[] = { 0.297254f,0.30829f,0.306678f,alpha };
GLfloat sphere_1_6_material_shininess = 0.1 * 128;



// 2 nd colume che
// ***** 1st sphere on 2nd column, brass *****
GLfloat sphere_2_1_material_ambient[] = { 0.329412f,0.223529f,0.027451f,alpha };
GLfloat sphere_2_1_material_diffuse[] = { 0.780392f,0.568627f,0.113725f,alpha };
GLfloat sphere_2_1_material_specular[] = { 0.992157f,0.941176f,0.807843f,alpha };
GLfloat sphere_2_1_material_shininess = 0.21794872 * 128;

// ***** 2nd sphere on 2nd column, bronze *****
GLfloat sphere_2_2_material_ambient[] = { 0.2125f,0.1275f,0.054f,alpha };
GLfloat sphere_2_2_material_diffuse[] = { 0.714f,0.4284f,0.18144f,alpha };
GLfloat sphere_2_2_material_specular[] = { 0.393548f,0.271906f,0.166721f,alpha };
GLfloat sphere_2_2_material_shininess = 0.2 * 128;

// ***** 3rd sphere on 2nd column, chrome *****
GLfloat sphere_2_3_material_ambient[] = { 0.25f,0.25f,0.25f,alpha };
GLfloat sphere_2_3_material_diffuse[] = { 0.4f,0.4f,0.4f,alpha };
GLfloat sphere_2_3_material_specular[] = { 0.774597f,0.774597f,0.774597f,alpha };
GLfloat sphere_2_3_material_shininess = 0.6 * 128;

// ***** 4th sphere on 2nd column, copper *****
GLfloat sphere_2_4_material_ambient[] = { 0.19125f,0.0735f,0.0225f,alpha };
GLfloat sphere_2_4_material_diffuse[] = { 0.7038f,0.27048f,0.0828f,alpha };
GLfloat sphere_2_4_material_specular[] = { 0.256777f,0.137622f,0.086014f,alpha };
GLfloat sphere_2_4_material_shininess = 0.1 * 128;

// ***** 5th sphere on 2nd column, gold *****
GLfloat sphere_2_5_material_ambient[] = { 0.24725f,0.1995f,0.0745f,alpha };
GLfloat sphere_2_5_material_diffuse[] = { 0.75164f,0.60648f,0.22648f,alpha };
GLfloat sphere_2_5_material_specular[] = { 0.628281f,0.555802f,0.366065f,alpha };
GLfloat sphere_2_5_material_shininess = 0.4 * 128;

// ***** 6th sphere on 2nd column, silver *****
GLfloat sphere_2_6_material_ambient[] = { 0.19225f,0.19225f,0.19225f,alpha };
GLfloat sphere_2_6_material_diffuse[] = { 0.50754f,0.50754f,0.50754f,alpha };
GLfloat sphere_2_6_material_specular[] = { 0.508273f,0.508273f,0.508273f,alpha };
GLfloat sphere_2_6_material_shininess = 0.4 * 128;

// 3rd column che

// ***** 1st sphere on 3rd column, black *****
GLfloat sphere_3_1_material_ambient[] = { 0.0f,0.0f,0.0f,alpha };
GLfloat sphere_3_1_material_diffuse[] = { 0.01f,0.01f,0.01f,alpha };
GLfloat sphere_3_1_material_specular[] = { 0.50f,0.50f,0.50f,alpha };
GLfloat sphere_3_1_material_shininess = 0.25 * 128;

// ***** 2nd sphere on 3rd column, cyan *****
GLfloat sphere_3_2_material_ambient[] = { 0.0f,0.1f,0.06f,alpha };
GLfloat sphere_3_2_material_diffuse[] = { 0.0f,0.50980392f,0.50980392f,alpha };
GLfloat sphere_3_2_material_specular[] = { 0.50196078f,0.50196078f,0.50196078f,alpha };
GLfloat sphere_3_2_material_shininess = 0.25 * 128;

// ***** 3rd sphere on 2nd column, green *****
GLfloat sphere_3_3_material_ambient[] = { 0.0f,0.0f,0.0f,alpha };
GLfloat sphere_3_3_material_diffuse[] = { 0.1f,0.35f,0.1f,alpha };
GLfloat sphere_3_3_material_specular[] = { 0.45f,0.55f,0.45f,alpha };
GLfloat sphere_3_3_material_shininess = 0.25 * 128;

// ***** 4th sphere on 3rd column, red *****
GLfloat sphere_3_4_material_ambient[] = { 0.0f,0.0f,0.0f,alpha };
GLfloat sphere_3_4_material_diffuse[] = { 0.5f,0.0f,0.0f,alpha };
GLfloat sphere_3_4_material_specular[] = { 0.7f,0.6f,0.6f,alpha };
GLfloat sphere_3_4_material_shininess = 0.25 * 128;

// ***** 5th sphere on 3rd column, white *****
GLfloat sphere_3_5_material_ambient[] = { 0.0f,0.0f,0.0f,alpha };
GLfloat sphere_3_5_material_diffuse[] = { 0.55f,0.55f,0.55f,alpha };
GLfloat sphere_3_5_material_specular[] = { 0.70f,0.70f,0.70f,alpha };
GLfloat sphere_3_5_material_shininess = 0.25 * 128;

// ***** 6th sphere on 3rd column, yellow plastic *****
GLfloat sphere_3_6_material_ambient[] = { 0.0f,0.0f,0.0f,alpha };
GLfloat sphere_3_6_material_diffuse[] = { 0.5f,0.5f,0.0f,alpha };
GLfloat sphere_3_6_material_specular[] = { 0.60f,0.60f,0.50f,alpha };
GLfloat sphere_3_6_material_shininess = 0.25 * 128;


// 4th column che

// ***** 1st sphere on 4th column, black *****
GLfloat sphere_4_1_material_ambient[] = { 0.02f,0.02f,0.02f,alpha };
GLfloat sphere_4_1_material_diffuse[] = { 0.1f,0.1f,0.1f,alpha };
GLfloat sphere_4_1_material_specular[] = { 0.4f,0.4f,0.4f,alpha };
GLfloat sphere_4_1_material_shininess = 0.078125 * 128;

// ***** 2nd sphere on 4th column, cyan *****
GLfloat sphere_4_2_material_ambient[] = { 0.0f,0.5f,0.5f,alpha };
GLfloat sphere_4_2_material_diffuse[] = { 0.4f,0.5f,0.5f,alpha };
GLfloat sphere_4_2_material_specular[] = { 0.04f,0.7f,0.7f,alpha };
GLfloat sphere_4_2_material_shininess = 0.078125 * 128;

// ***** 3rd sphere on 4th column, green *****
GLfloat sphere_4_3_material_ambient[] = { 0.0f,0.05f,0.0f,alpha };
GLfloat sphere_4_3_material_diffuse[] = { 0.4f,0.5f,0.4f,alpha };
GLfloat sphere_4_3_material_specular[] = { 0.04f,0.7f,0.04f,alpha };
GLfloat sphere_4_3_material_shininess = 0.078125 * 128;

// ***** 4th sphere on 4th column, red *****
GLfloat sphere_4_4_material_ambient[] = { 0.5f,0.0f,0.0f,alpha };
GLfloat sphere_4_4_material_diffuse[] = { 0.5f,0.4f,0.4f,alpha };
GLfloat sphere_4_4_material_specular[] = { 0.7f,0.04f,0.04f,alpha };
GLfloat sphere_4_4_material_shininess = 0.078125 * 128;

// ***** 5th sphere on 4th column, white *****
GLfloat sphere_4_5_material_ambient[] = { 0.5f,0.5f,0.5f,alpha };
GLfloat sphere_4_5_material_diffuse[] = { 0.5f,0.5f,0.5f,alpha };
GLfloat sphere_4_5_material_specular[] = { 0.7f,0.7f,0.7f,alpha };
GLfloat sphere_4_5_material_shininess = 0.078125 * 128;

// ***** 6th sphere on 4th column, yellow rubber *****
GLfloat sphere_4_6_material_ambient[] = { 0.05f,0.05f,0.0f,alpha };
GLfloat sphere_4_6_material_diffuse[] = { 0.5f,0.5f,0.4f,alpha };
GLfloat sphere_4_6_material_specular[] = { 0.7f,0.7f,0.04f,alpha };
GLfloat sphere_4_6_material_shininess = 0.078125 * 128;

// global arrays for lights effect
GLfloat light_model_ambient[] = {0.2f,0.2f,0.2f,0.0f};
GLfloat light_model_local_viewer[] = {0.0f};



//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display();
	void update();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline Using Native Windowing : gluLookAt"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
				display();
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C: // l button handled
			//MessageBox(hwnd, TEXT("Message"), TEXT("Message"), LB_OKAY);
			if (gbIsLight == false)
			{
				glEnable(GL_LIGHTING);
				gbIsLight = true;
			}
			else
			{
				glDisable(GL_LIGHTING);
				gbIsLight = false;
			}
			break;
		case 0x58:
			//MessageBox(hwnd, TEXT("Message"), TEXT("X"), LB_OKAY);

			if (gbIsXKeyPressed == GL_FALSE)
			{
				//gLightAngle = 0.0f;
				gbIsXKeyPressed = GL_TRUE;
				gbIsYKeyPressed = GL_FALSE;
				gbIsZKeyPressed = GL_FALSE;
			}
			break;
		case 0x59:
			//MessageBox(hwnd, TEXT("Message"), TEXT("Y"), LB_OKAY);
			if (gbIsYKeyPressed == GL_FALSE)
			{
				gLightAngle = 0.0f;
				gbIsYKeyPressed = GL_TRUE;
				gbIsXKeyPressed = GL_FALSE;
				gbIsZKeyPressed = GL_FALSE;
			}
			break;
		case 0x5A:
			//MessageBox(hwnd, TEXT("Message"), TEXT("Z"), LB_OKAY);
			if (gbIsZKeyPressed == GL_FALSE)
			{
				gLightAngle = 0.0f;
				gbIsZKeyPressed = GL_TRUE;
				gbIsXKeyPressed = GL_FALSE;
				gbIsYKeyPressed = GL_FALSE;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | 
			SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// changes for lighting
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffused);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	//glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glEnable(GL_LIGHT0);


	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	void drawSphere();
	void animateLights();

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
		gluLookAt(0.0f, 0.0f, 0.1f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

		animateLights();

		// ***** 1st sphere on 1st column, emerald *****
		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_1_1_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_1_1_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_1_1_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_1_1_material_shininess);
		glTranslatef(-4.0f, 6.0f, -17.0f);
		//glTranslatef(0.0f, 0.0f, -3.0f);
		drawSphere();
		glPopMatrix();


		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_2_1_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_2_1_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_2_1_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_2_1_material_shininess);
		glTranslatef(-1.0f, 6.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_3_1_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_3_1_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_3_1_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_3_1_material_shininess);
		glTranslatef(2.0f, 6.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_4_1_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_4_1_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_4_1_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_4_1_material_shininess);
		glTranslatef(5.0f, 6.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		// second row
		// ***** 2nd sphere on 1st column, jade *****
		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_1_2_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_1_2_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_1_2_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_1_2_material_shininess);
		glTranslatef(-4.0f, 4.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_2_2_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_2_2_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_2_2_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_2_2_material_shininess);
		glTranslatef(-1.0f, 4.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_3_2_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_3_2_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_3_2_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_3_2_material_shininess);
		glTranslatef(2.0f, 4.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_4_2_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_4_2_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_4_2_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_4_2_material_shininess);
		glTranslatef(5.0f, 4.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		// third row
		// ***** 3rd sphere on 1st column, obsidian *****
		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_1_3_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_1_3_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_1_3_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_1_3_material_shininess);
		glTranslatef(-4.0f, 2.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_2_3_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_2_3_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_2_3_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_2_3_material_shininess);
		glTranslatef(-1.0f, 2.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_3_3_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_3_3_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_3_3_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_3_3_material_shininess);
		glTranslatef(2.0f, 2.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_4_3_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_4_3_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_4_3_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_4_3_material_shininess);
		glTranslatef(5.0f, 2.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		// fourth row

		// ***** 4th sphere on 1st column, pearl *****
		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_1_4_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_1_4_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_1_4_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_1_4_material_shininess);
		glTranslatef(-4.0f, 0.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_2_4_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_2_4_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_2_4_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_2_4_material_shininess);
		glTranslatef(-1.0f, 0.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_3_4_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_3_4_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_3_4_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_3_4_material_shininess);
		glTranslatef(2.0f, 0.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_4_4_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_4_4_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_4_4_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_4_4_material_shininess);
		glTranslatef(5.0f, 0.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		// fifth row

		// ***** 5th sphere on 1st column, ruby *****
		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_1_5_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_1_5_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_1_5_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_1_5_material_shininess);
		glTranslatef(-4.0f, -2.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_2_5_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_2_5_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_2_5_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_2_5_material_shininess);
		glTranslatef(-1.0f, -2.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_3_5_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_3_5_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_3_5_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_3_5_material_shininess);
		glTranslatef(2.0f, -2.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_4_5_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_4_5_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_4_5_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_4_5_material_shininess);
		glTranslatef(5.0f, -2.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		// sixth row

		// ***** 6th sphere on 1st column, turquoise *****
		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_1_6_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_1_6_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_1_6_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_1_6_material_shininess);
		glTranslatef(-4.0f, -4.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_2_6_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_2_6_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_2_6_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_2_6_material_shininess);
		glTranslatef(-1.0f, -4.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_3_6_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_3_6_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_3_6_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_3_6_material_shininess);
		glTranslatef(2.0f, -4.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPushMatrix();
		glMaterialfv(GL_FRONT, GL_AMBIENT, sphere_4_6_material_ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, sphere_4_6_material_diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, sphere_4_6_material_specular);
		glMaterialf(GL_FRONT, GL_SHININESS, sphere_4_6_material_shininess);
		glTranslatef(5.0f, -4.0f, -17.0f);
		drawSphere();
		glPopMatrix();

		glPopMatrix();

		//animateLights();

	SwapBuffers(ghdc);
}

void animateLights()
{
	glPushMatrix();

		if (gbIsXKeyPressed)
		{
		
			glRotatef(gLightAngle, 1.0f, 0.0f, 0.0f);

			light_position[1] = gLightAngle;
			light_position[0] = 0.0f;
			light_position[2] = 0.0f;
		}

		else if (gbIsYKeyPressed)
		{

			glRotatef(gLightAngle, 0.0f, 1.0f, 0.0f);

			light_position[0] = gLightAngle;
			light_position[1] = 0.0f;
			light_position[2] = 0.0f;
		}

		else if (gbIsZKeyPressed)
		{
			
			glRotatef(gLightAngle, 0.0f, 0.0f, 1.0f);

			light_position[0] = gLightAngle;
			light_position[1] = 0.0f;
			light_position[2] = gLightAngle;

		}

	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glPopMatrix();
}

void drawSphere()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	//glColor3f(1.0f, 1.0f, 0.0f);

	gluSphere(quadric, 0.75f, 50, 50);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void update()
{
	gLightAngle = gLightAngle + 0.4f;
	if (gLightAngle >= 360.0f)
	{
		gLightAngle = 0.0f;
	}
}
