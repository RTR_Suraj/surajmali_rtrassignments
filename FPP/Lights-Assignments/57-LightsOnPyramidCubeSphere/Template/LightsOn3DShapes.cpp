#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define PI 3.1415926535898

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLfloat anglePyramid = 0.0f;
GLfloat angleCube = 0.0f;
GLfloat angleShpere = 0.0f;

bool gbIsLightingEnable = false;

GLUquadric *quadric;

// arrays required for lighting effect

// left side cha light - BLUE
GLfloat lights_0_ambient[]	=	{0.0f,0.0f,0.0f,0.0f};
GLfloat lights_0_diffused[]	=	{0.0f,0.0f,1.0f,1.0f};
GLfloat lights_0_specular[]	=	{0.0f,0.0f,1.0f,1.0f};
GLfloat lights_0_position[]	=	{-2.0f,1.0f,1.0f,0.0f};
GLfloat lights_0_shininess[] =	{ 50.0f,50.0f,50.0f,50.0f };

// right side cha light - RED
GLfloat lights_1_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat lights_1_diffused[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lights_1_specular[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lights_1_position[] = { 2.0f,1.0f,1.0f,0.0f };
GLfloat lights_1_shininess[] = { 50.0f,50.0f,50.0f,50.0f };


GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffused[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess[] = { 50.0f,50.0f,50.0f,50.0f };

GLboolean gbIsPyramid = GL_TRUE;
GLboolean gbIsCube = GL_FALSE;
GLboolean gbIsSphere = GL_FALSE;



//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display();
	void update();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline Using Native Windowing : Point at"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
				display();
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C: // handling of l key
			if (gbIsLightingEnable == false)
			{
				glEnable(GL_LIGHTING);
				gbIsLightingEnable = true;
			}
			else
			{
				glDisable(GL_LIGHTING);
				gbIsLightingEnable = false;
			}
			break;
		case 0x50: // P key
			if (gbIsPyramid == GL_FALSE)
			{
				gbIsPyramid = GL_TRUE;
				gbIsCube = GL_FALSE;
				gbIsSphere = GL_FALSE;
			}
			break;
		case 0x43: // C key
			if (gbIsCube == GL_FALSE)
			{
				gbIsCube = GL_TRUE;
				gbIsPyramid = GL_FALSE;
				gbIsSphere = GL_FALSE;
			}
			break;
		case 0x53: // S key
			if (gbIsSphere == GL_FALSE)
			{
				gbIsSphere = GL_TRUE;
				gbIsPyramid = GL_FALSE;
				gbIsCube = GL_FALSE;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// calls required for lighting

	// right side cha red light
	glLightfv(GL_LIGHT0, GL_AMBIENT, lights_1_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lights_1_diffused);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lights_1_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, lights_1_position);
	glLightfv(GL_LIGHT0, GL_SHININESS, lights_1_shininess);

	// left side cha blue light
	glLightfv(GL_LIGHT1, GL_AMBIENT, lights_0_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lights_0_diffused);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lights_0_specular);
	glLightfv(GL_LIGHT1, GL_POSITION, lights_0_position);
	glLightfv(GL_LIGHT1, GL_SHININESS, lights_0_shininess);

	// material
	glMaterialfv(GL_FRONT,GL_AMBIENT,material_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_diffused);
	glMaterialfv(GL_FRONT, GL_SPECULAR, material_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);
	

	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{

	void drawPyramid();
	void drawCube();
	void drawSpehere();

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (gbIsPyramid == GL_TRUE)
	{
		glTranslatef(0.0f, 0.0f, -6.0f);
		glRotatef(anglePyramid, 0.0f, 1.0f, 0.0f);
		drawPyramid();
	}
	else if (gbIsCube == GL_TRUE)
	{
		// reset to origin
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glTranslatef(0.0f, 0.0f, -6.0f);
		glScalef(0.75f, 0.75f, 0.75f);
		glRotatef(angleCube, 0.0f, 1.0f, 0.0f);
		drawCube();
	}
	else if (gbIsSphere == GL_TRUE)
	{
		glTranslatef(0.0f, 0.0f, -2.0f);
		glRotatef(angleShpere, 0.0f, 1.0f, 0.0f);
		drawSpehere();
	}
	SwapBuffers(ghdc);
}

void drawSpehere()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();
	//glColor3f(1.0f, 1.0f, 0.0f);

	gluSphere(quadric, 0.75, 30, 30);
}

void drawPyramid()
{
	glBegin(GL_TRIANGLES);

	// front face of pyramid
	glNormal3f(0.0f,0.447214f,0.894427f);
	//glColor3f(1.0f, 1.0f, 1.0f); // white color
	glVertex3f(0.0f, 1.0f, 0.0f); // origin (apex of triangle)

	//glColor3f(0.0f, 1.0f, 0.0f); // green color
	glVertex3f(-1.0f, -1.0f, 1.0f); // left corner vertex

	// right face
	//glColor3f(0.0f, 0.0f, 1.0f); // blue color
	glVertex3f(1.0f, -1.0f, 1.0f);// right corner vertex


	//right face of pyramid
	//glColor3f(1.0f, 0.0f, 0.0f); // red color
	glNormal3f(0.894427f, 0.447214f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // origin (apex of triangle)

	//glColor3f(0.0f, 0.0f, 1.0f); // blue color
	glVertex3f(1.0f, -1.0f, 1.0f);// right face left corner vertex

	//glColor3f(0.0f, 1.0f, 0.0f); // grren color
	glVertex3f(1.0f, -1.0f, -1.0f);// right face backside corner vertex

	//back face of pyramid
	//glColor3f(1.0f, 0.0f, 0.0f); // red color
	glNormal3f(0.0f, 0.447214f, -1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // origin (apex of triangle)

	//glColor3f(0.0f, 1.0f, 0.0f); // green color
	glVertex3f(1.0f, -1.0f, -1.0f); // left corner of back face

	//glColor3f(0.0f, 0.0f, 1.0f); // blue color
	glVertex3f(-1.0f, -1.0f, -1.0f); // right corner of back face

	// left  face of pyramid
	//glColor3f(1.0f, 0.0f, 0.0f); // red color
	glNormal3f(-0.894427f, 0.447214f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f); // origin (apex of triangle)

	//glColor3f(0.0f, 0.0f, 1.0f); // blue color
	glVertex3f(-1.0f, -1.0f, -1.0f); // left corner of left face

	//glColor3f(0.0f, 1.0f, 0.0f); // green color
	glVertex3f(-1.0f, -1.0f, 1.0f); // right corner of left face


	glEnd();
}

void drawCube()
{
	glBegin(GL_QUADS);

	// front face
	//glColor3f(1.0f, 0.0f, 0.0f); // red color
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// right face
	//glColor3f(0.0f, 1.0f, 0.0f); // green color
	glNormal3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f); // right top of right face
	glVertex3f(1.0f, 1.0f, 1.0f); // left top of right face
	glVertex3f(1.0f, -1.0f, 1.0f); // left bottom of right face
	glVertex3f(1.0f, -1.0f, -1.0f); // right bottom of right face

	// back face
	//glColor3f(0.0f, 0.0f, 1.0f); // blue color
	glNormal3f(0.0f, 0.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f); // right top of back face
	glVertex3f(1.0f, 1.0f, -1.0f); // left top of back face
	glVertex3f(1.0f, -1.0f, -1.0f); // left bottom of back face
	glVertex3f(-1.0f, -1.0f, -1.0f); // right bottom of back face

	// left face
	//glColor3f(1.0f, 1.0f, 0.0f); // red color
	glNormal3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f); // right top of left face
	glVertex3f(-1.0f, 1.0f, -1.0f); // left top of left face
	glVertex3f(-1.0f, -1.0f, -1.0f); // left bottom of left face
	glVertex3f(-1.0f, -1.0f, 1.0f); // right bottom of left face

	// top face
	//glColor3f(1.0f, 0.0f, 1.0f); // green color
	glNormal3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f); // right top of top face
	glVertex3f(-1.0f, 1.0f, -1.0f); // left top of top facere4e3
	glVertex3f(-1.0f, 1.0f, 1.0f); // left bottom of top face
	glVertex3f(1.0f, 1.0f, 1.0f); // right bottom of top face

	// bottom face
	//glColor3f(0.0f, 1.0f, 1.0f); // green color
	glNormal3f(0.0f, -1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f); // right top of bottom face
	glVertex3f(-1.0f, -1.0f, 1.0f); // left top of bottom face
	glVertex3f(-1.0f, -1.0f, -1.0f); // left bottom of bottom face
	glVertex3f(1.0f, -1.0f, -1.0f); // right bottom of bottom face


	glEnd();
}


void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}

void update()
{
	anglePyramid = anglePyramid + 0.05f;
	if (anglePyramid >= 360.0f)
	{
		anglePyramid = 0.0f;
	}

	angleCube = angleCube + 0.05f;
	if (angleCube <= 0)
	{
		angleCube = 0.0f;
	}

	angleShpere = angleShpere + 0.05f;
	if (angleShpere <= 0)
	{
		angleShpere = 0.0f;
	}
}
