#include <windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define PI 3.1415926535898

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Shapes");
	bool bDone = false;

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline Using Native Windowing : Shapes "),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				display();
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{

	void drawPoints();
	void drawThirdShape();
	void drawFourthShape();
	void drawSecondShape();
	void drawFifthShape();
	void drawSixthShape();

	//code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-3.0f, 0.0f, -5.0f);
	glScalef(1.2f, 1.2f, 0.0f);
	drawPoints();

	// second shape
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.8f, 0.0f, -5.0f);
	glScalef(1.3f, 1.3f, 0.0f);
	drawSecondShape();

	// third shape
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, 0.0f, -5.0f);
	glScalef(1.3f, 1.3f, 0.0f);
	drawThirdShape();

	// fourth shape
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-3.0f, -1.5f, -5.0f);
	glScalef(1.3f, 1.3f, 0.0f);
	drawFourthShape();

	// fifth shape
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-0.8f, -1.5f, -5.0f);
	glScalef(1.3f, 1.3f, 0.0f);
	drawFifthShape();

	// sixth shape
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, -1.5f, -5.0f);
	glScalef(1.3f, 1.3f, 0.0f);
	drawSixthShape();

	SwapBuffers(ghdc);
}

void drawPoints()
{
	glPointSize(3.0f);
	glBegin(GL_POINTS);
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex2f(0.80f, 0.80f);
	glVertex2f(0.60f, 0.80f);
	glVertex2f(0.40f, 0.80f);
	glVertex2f(0.20f, 0.80f);

	glVertex2f(0.80f, 0.60f);
	glVertex2f(0.60f, 0.60f);
	glVertex2f(0.40f, 0.60f);
	glVertex2f(0.20f, 0.60f);

	glVertex2f(0.80f, 0.40f);
	glVertex2f(0.60f, 0.40f);
	glVertex2f(0.40f, 0.40f);
	glVertex2f(0.20f, 0.40f);

	glVertex2f(0.80f, 0.20f);
	glVertex2f(0.60f, 0.20f);
	glVertex2f(0.40f, 0.20f);
	glVertex2f(0.20f, 0.20f);

	glEnd();
}

void drawThirdShape()
{
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);

	glVertex2f(0.20f, 0.80f);
	glVertex2f(0.80f, 0.80f);
	glVertex2f(0.80f, 0.20f);
	glVertex2f(0.20f, 0.20f);

	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.60f, 0.80f);
	glVertex2f(0.60f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.40f, 0.80f);
	glVertex2f(0.40f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.20f, 0.60f);
	glVertex2f(0.80f, 0.60f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.20f, 0.40f);
	glVertex2f(0.80f, 0.40f);
	glEnd();

}

void drawFourthShape()
{
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);

	glVertex2f(0.20f, 0.80f);
	glVertex2f(0.80f, 0.80f);
	glVertex2f(0.80f, 0.20f);
	glVertex2f(0.20f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.60f, 0.80f);
	glVertex2f(0.60f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.40f, 0.80f);
	glVertex2f(0.40f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.20f, 0.60f);
	glVertex2f(0.80f, 0.60f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.20f, 0.40f);
	glVertex2f(0.80f, 0.40f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.40f, 0.80f);
	glVertex2f(0.20f, 0.60f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.60f, 0.80f);
	glVertex2f(0.20f, 0.40f);
	glEnd();

	// centre diagonal 
	glBegin(GL_LINES);
	glVertex2f(0.80f, 0.80f);
	glVertex2f(0.20f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.80f, 0.60f);
	glVertex2f(0.40f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.80f, 0.40f);
	glVertex2f(0.60f, 0.20f);
	glEnd();
}

void drawSecondShape()
{
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);

	glVertex2f(0.20f, 0.80f);
	glVertex2f(0.80f, 0.80f);
	glVertex2f(0.20f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.60f, 0.80f);
	glVertex2f(0.60f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.40f, 0.80f);
	glVertex2f(0.40f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.20f, 0.60f);
	glVertex2f(0.80f, 0.60f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.20f, 0.40f);
	glVertex2f(0.80f, 0.40f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.40f, 0.80f);
	glVertex2f(0.20f, 0.60f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.60f, 0.80f);
	glVertex2f(0.20f, 0.40f);
	glEnd();

	// centre diagonal 
	glBegin(GL_LINES);
	glVertex2f(0.80f, 0.80f);
	glVertex2f(0.20f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.80f, 0.60f);
	glVertex2f(0.40f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.80f, 0.40f);
	glVertex2f(0.60f, 0.20f);
	glEnd();
}

void drawFifthShape()
{
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);

	glVertex2f(0.20f, 0.80f);
	glVertex2f(0.80f, 0.80f);
	glVertex2f(0.80f, 0.20f);
	glVertex2f(0.20f, 0.20f);
	glEnd();

	// first to eigth point(as per colum based matrix(no actual relation with matrix , thi is just to understand later))
	glBegin(GL_LINES);
	glVertex2f(0.20f, 0.80f);
	glVertex2f(0.40f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.20f, 0.80f);
	glVertex2f(0.40f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.20f, 0.80f);
	glVertex2f(0.60f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.20f, 0.80f);
	glVertex2f(0.80f, 0.20f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.20f, 0.80f);
	glVertex2f(0.80f, 0.40f);
	glEnd();

	glBegin(GL_LINES);
	glVertex2f(0.20f, 0.80f);
	glVertex2f(0.80f, 0.60f);
	glEnd();


}

void drawSixthShape()
{
	glLineWidth(1.0f);
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex2f(0.80f, 0.80f);
	glVertex2f(0.18f, 0.80f);
	glVertex2f(0.18f, 0.185f);
	glVertex2f(0.80f, 0.185f);

	glEnd();

	// right top square
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex2f(0.80f, 0.80f);
	glVertex2f(0.60f, 0.80f);
	glVertex2f(0.60f, 0.60f);
	glVertex2f(0.80f, 0.60f);
	
	glEnd();

	// right middle square
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex2f(0.60f, 0.595f);
	glVertex2f(0.80f, 0.595f);
	glVertex2f(0.80f, 0.395f);
	glVertex2f(0.60f, 0.395f);
	
	glEnd();

	// right bottom square
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex2f(0.80f, 0.385f);
	glVertex2f(0.60f, 0.385f);
	glVertex2f(0.60f, 0.185f);
	glVertex2f(0.80f, 0.185f);

	glEnd();

	// middle top square
	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);

	glVertex2f(0.59f, 0.80f);
	glVertex2f(0.39f, 0.80f);
	glVertex2f(0.39f, 0.60f);
	glVertex2f(0.59f, 0.60f);

	glEnd();

	// middle middle square
	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);

	glVertex2f(0.39f, 0.595f);
	glVertex2f(0.59f, 0.595f);
	glVertex2f(0.59f, 0.395f);
	glVertex2f(0.39f, 0.395f);

	glEnd();

	// middle bottom square
	glBegin(GL_QUADS);
	glColor3f(0.0f, 1.0f, 0.0f);

	glVertex2f(0.59f, 0.385f);
	glVertex2f(0.39f, 0.385f);
	glVertex2f(0.39f, 0.185f);
	glVertex2f(0.59f, 0.185f);

	glEnd();

	// left top square
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);

	glVertex2f(0.38f, 0.80f);
	glVertex2f(0.18f, 0.80f);
	glVertex2f(0.18f, 0.60f);
	glVertex2f(0.38f, 0.60f);

	glEnd();

	// left middle square
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);

	glVertex2f(0.18f, 0.595f);
	glVertex2f(0.38f, 0.595f);
	glVertex2f(0.38f, 0.395f);
	glVertex2f(0.18f, 0.395f);

	glEnd();

	// left bottom square
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);

	glVertex2f(0.38f, 0.385f);
	glVertex2f(0.18f, 0.385f);
	glVertex2f(0.18f, 0.185f);
	glVertex2f(0.38f, 0.185f);

	glEnd();
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)width/ (GLfloat)height,0.1f,100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}
