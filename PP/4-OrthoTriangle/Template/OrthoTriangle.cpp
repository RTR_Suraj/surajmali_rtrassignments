#include <windows.h>
#include<iostream>

#include<gl/glew.h> //IMPORTANT : This MUST Be Done Before #include For gl\GL.h

#include <gl/GL.h>
#include <gl/GLU.h>
#include "vmath.h"

using namespace vmath;


#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define PI 3.1415926535898

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glew32.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

// shader related vars
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

FILE *gpFile = NULL;

GLuint gVao;
GLuint gVbo;
GLuint gMVPUniform;

mat4 gOrthographicProjectionMatrix;

enum 
{
	SBM_ATTRIBUTE_VERTEX = 0,
	SBM_ATTRIBUTE_COLOR,
	SBM_ATTRIBUTE_NORMAL,
	SBM_ATTRIBUTE_TEXTURE0
};


//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL PP code: Triangle ortho "),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				display();
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, 
					mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, 
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | 
			SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);
	void uninitialize();

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	
	fopen_s(&gpFile, "Log.txt", "w");

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 24;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// code related to PP ie glewInit
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// get GL_VERSION and GL_SHADING_LANG_VERSION to define them in #version tag in shader source code
	fprintf(gpFile, "GL version: %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//******* VERTEX SHADER *************
	// Steps for using a shader in our code
	/*
	1. Create the shader by providing the argument name of the shader like GL_VERTEX_SHADER,GL_FRAGMENT_SHADER 
	2. write source code for the shader as a string with back slash(\) at each line end
	3. provide this shader source
	4. compile the shader source
	5. link the shader source 
	6. use it for our drawing
	*/

	//1. Create the shader by providing the argument name of the shader like GL_VERTEX_SHADER,GL_FRAGMENT_SHADER  - glCreateShader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//2. write source code for the shader as a string with back slash(\)
	const GLchar *vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"}";

	//3. provide shader source - glShaderSource()
	glShaderSource(gVertexShaderObject, // Specifies the handle of the shader object whose source code is to be replaced.
		1,								// Specifies the number of elements in the string and length arrays.
		&vertexShaderSourceCode,		// Specifies an array of pointers to strings containing the source code to be loaded into the shader.
		NULL);							// Specifies an array of string lengths.

	//4. compile the shader source
	glCompileShader(gVertexShaderObject);

	// 5. finding error while compilation
	/*
	There might be possibilty in compile time error of our shader source code
	We can fetch these errors using glGetShaderiv API
	We are using this API to get last error from compilation process
	Process to get last error line :
	1. glGetShaderiv gives us the compile status code(GLint), so get it
	2. check it with GL_TRUE(success)/ GL_FALSE(error)
	3. if this is GL_FALSE - 
		1. Get log information line length
		2. allocate a memory block of char* size to get this error into
		3. using glGetShaderInfoLog API - get actual error line in above step varibale
		4. prin it and exit from code calling uninitialise()
	*/

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	// get compile status
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE)
	{
		// get info log lengh
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			// allocate memory to hold the error
			szInfoLog = (char *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				// get actual last error line
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Vertex shader compilaton log : %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//******** FRAGMENT SHADER *********
	// create the shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// write the shader source
	const GLchar *fragmentShaderSource =
		"#version 430 core" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{"\
		"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		"}";

	// provide the source code
	glShaderSource(gFragmentShaderObject, 1, &fragmentShaderSource, NULL);

	// compile the shader source
	glCompileShader(gFragmentShaderObject);

	// 5. finding error while compilation
	/*
	There might be possibilty in compile time error of our shader source code
	We can fetch these errors using glGetShaderiv API
	We are using this API to get last error from compilation process
	Process to get last error line :
	1. glGetShaderiv gives us the compile status code(GLint), so get it
	2. check it with GL_TRUE(success)/ GL_FALSE(error)
	3. if this is GL_FALSE -
		1. Get log information line length
		2. allocate a memory block of char* size to get this error into
		3. using glGetShaderInfoLog API - get actual error line in above step varibale
		4. prin it and exit from code calling uninitialise()
	*/

	// get compiled status
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Fragment shader compilaton log : %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}



	// ******* SHADER PROGRAM *******
	/*
	We need to attach these compiled source codes to a shader program
	We need to create a shader program for this operation
	Steps
	1. Create the shader program
	2. attach each shader object with the shader program created above
	3. And link this program object for further use
	*/
	//1. Create the shader program
	gShaderProgramObject = glCreateProgram();

	//2. attach each shader object with the shader program created above
	// attaching vertex shader object
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attaching fragment shader object
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// before linking of shader code, we should bind shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, SBM_ATTRIBUTE_VERTEX , "vPosition");

	// LINK the program object
	glLinkProgram(gShaderProgramObject);

	// find out linking time errors
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// after linking - get the uniform , allocate a space to it so that at runtime we can use this space to keep
	// out matrix multiplication into it
	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");


	/*
	vertices, colors,shader attribs,vao,vbo initializations
	*/

	const GLfloat triangleVertices[] =
	{
		0.0f,50.0f,0.0f,	// apex
		-50.0f,-50.0f,0.0f,	// left bottom
		50.0f,-50.0f,0.0f	// right bottom
	};

	glGenVertexArrays(1, &gVao);
	glBindVertexArray(gVao);

	glGenBuffers(1, &gVbo);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(SBM_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(SBM_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	// we are using this linked program object in our display while rendering objects
	// openGL related code
	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

	glShadeModel(GL_SMOOTH);
	// depth settings
	// set up depth buffer
	glClearDepth(1.0f);
	// enable depth test
	glEnable(GL_DEPTH_TEST);
	// depth test to do work
	glDepthFunc(GL_LEQUAL);
	// set nice perspective calculations
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	// alwasy cull the back faces wo perform better
	glEnable(GL_CULL_FACE);


	// set orthographic projection matrix to identity
	gOrthographicProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// start using shader program object
	glUseProgram(gShaderProgramObject);

	// do the drawing ...

	// set modelview and model view projection matrices to identity
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	// multiply the modelview and orthographic projection matrix to get modelViewProjectionMatrix
	modelViewProjectionMatrix = gOrthographicProjectionMatrix * modelViewMatrix; // ORDER is IMP

	// pass this modelViewProjectionMatrix tovertex shader in "u_mvp_matrix" shader variable
	// whose position value we already calculated in initialise() -  gMVPUniform

	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	/// bind the vao
	glBindVertexArray(gVao);

	// draw the triangle
	glDrawArrays(GL_TRIANGLES, 0, 3);

	// unbind vao
	glBindVertexArray(0);

	// after drawing stop using shader program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}



void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	if (width <= height)
	{
		gOrthographicProjectionMatrix = ortho(-100.0f, 100.0f, (-100.0f * (height / width)), (100.0f * (height / width)),
			-100.0f, 100.0f);
	}
	else
	{
		gOrthographicProjectionMatrix = ortho((-100.0f * (width/height)), (100.0f * (width / height)), -100.0f, 100.0f,
			-100.0f, 100.0f);
	}
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | 
			SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao)
	{
		glDeleteVertexArrays(1, &gVao);
		gVao = 0;
	}

	// destroy vbo
	if (gVbo)
	{
		glDeleteVertexArrays(1, &gVbo);
		gVbo = 0;
	}

	// detach vertex shader
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Closing file !!!");
		fclose(gpFile);
		gpFile = NULL;
	}
}
