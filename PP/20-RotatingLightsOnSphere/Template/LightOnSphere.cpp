#include <windows.h>
#include<iostream>

#include<gl/glew.h> //IMPORTANT : This MUST Be Done Before #include For gl\GL.h

#include <gl/GL.h>
#include <gl/GLU.h>
#include "vmath.h"

#include "Sphere.h"

using namespace vmath;

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define PI 3.1415926535898

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"Sphere.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

// shader related vars
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

FILE *gpFile = NULL;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform;
GLuint view_matrix_uniform;
GLuint projection_matrix_uniform;
GLuint L_Key_PressedUniform;
GLuint F_Key_PressedUniform;
GLuint V_Key_PressedUniform;

GLuint La_red_uniform;
GLuint Ld_red_uniform;
GLuint Ls_red_uniform;
GLuint Light_Position_red_uniform;

GLuint La_green_uniform;
GLuint Ld_green_uniform;
GLuint Ls_green_uniform;
GLuint Light_Position_green_uniform;

GLuint La_blue_uniform;
GLuint Ld_blue_uniform;
GLuint Ls_blue_uniform;
GLuint Light_Position_blue_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;


// Lights of RED color
GLfloat red_lights_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat red_lights_diffuse[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat red_lights_specular[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat red_lights_position[] = { 0.0f,0.0f,0.0f,0.0f };

// Lights of GREEN color
GLfloat green_lights_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat green_lights_diffuse[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat green_lights_specular[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat green_lights_position[] = { 0.0f,0.0f,0.0f,0.0f };

// Lights of BLUE color
GLfloat blue_lights_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat blue_lights_diffuse[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat blue_lights_specular[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat blue_lights_position[] = { 0.0f,0.0f,0.0f,0.0f };

// material properties
GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffused[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess =  50.0f;

// variables for lights animation
GLfloat gRedLightAngle = 0.0f;
GLfloat gGreenLightAngle = 0.0f;
GLfloat gBlueLightAngle = 0.0f;

mat4 gPerspectiveProjectionMatrix;

bool gIsLKeyPressed = false;
bool gbLightEnabled = false;

bool gIsFKeyPressed = false;
bool gbPerFragmentLightEnabled = false;

bool gIsVKeyPressed = false;
bool gbPerVertexLightEnabled = false;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gNumElements;


enum
{
	SBM_ATTRIBUTE_VERTEX = 0,
	SBM_ATTRIBUTE_COLOR,
	SBM_ATTRIBUTE_NORMAL,
	SBM_ATTRIBUTE_TEXTURE0
};


//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display();
	void update();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL PP code: 3D Rotation of pyramid and cube "),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
				display();
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x46: //for 'f' or 'F' // enable per fragment lighting
			if (gIsFKeyPressed == false)
			{
				gIsFKeyPressed = true;
				gbPerVertexLightEnabled = false;
				gbPerFragmentLightEnabled = true;
				gIsVKeyPressed = false;
			}
			break;
		case 0x56: //for 'v'  // enable per fragment lighting
			if (gIsVKeyPressed == false)
			{
				gIsVKeyPressed = true;
				gbPerVertexLightEnabled = true;
				gbPerFragmentLightEnabled = false;
				gIsFKeyPressed = false;
			}
			break;
		case 0x4C: // // enable per fragment lighting for l
			if (gIsLKeyPressed  == false)
			{
				gbLightEnabled = true;
				gIsLKeyPressed = true;
			}
			else
			{
				gbLightEnabled = false;
				gIsLKeyPressed = false;
			}
			break;
		case 0x51: // Q key
			gbEscapeKeyIsPressed = true;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE |
			SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);
	void uninitialize();

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	fopen_s(&gpFile, "Log.txt", "w");

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// code related to PP ie glewInit
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// get GL_VERSION and GL_SHADING_LANG_VERSION to define them in #version tag in shader source code
	fprintf(gpFile, "GL version: %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//******* VERTEX SHADER *************
	// Steps for using a shader in our code
	/*
	1. Create the shader by providing the argument name of the shader like GL_VERTEX_SHADER,GL_FRAGMENT_SHADER
	2. write source code for the shader as a string with back slash(\) at each line end
	3. provide this shader source
	4. compile the shader source
	5. link the shader source
	6. use it for our drawing
	*/

	//1. Create the shader by providing the argument name of the shader like GL_VERTEX_SHADER,GL_FRAGMENT_SHADER  - glCreateShader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//2. write source code for the shader as a string with back slash(\)
	const GLchar *vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 uModelMatrix;" \
		"uniform mat4 uViewMatrix;" \
		"uniform mat4 uProjectionMatrix;" \
		"uniform vec3 uLaRed;" \
		"uniform vec3 uLdRed;" \
		"uniform vec3 uLsRed;" \
		"uniform vec3 uLaGreen;" \
		"uniform vec3 uLdGreen;" \
		"uniform vec3 uLsGreen;" \
		"uniform vec3 uLaBlue;" \
		"uniform vec3 uLdBlue;" \
		"uniform vec3 uLsBlue;" \
		"uniform vec3 uKa;" \
		"uniform vec3 uKd;" \
		"uniform vec3 uKs;" \
		"uniform int uLKeyPressed;" \
		"uniform int uFKeyPressed;" \
		"uniform int uVKeyPressed;" \
		"uniform vec4 uLightPositionRed;" \
		"uniform vec4 uLightPositionGreen;" \
		"uniform vec4 uLightPositionBlue;" \
		"uniform float uMaterialShininess;" \
		"out vec3 phongAdsColor;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction_red;" \
		"out vec3 light_direction_green;" \
		"out vec3 light_direction_blue;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(uVKeyPressed == 1)" \
		"{" \
			"vec4 eyeCordinates = uViewMatrix * uModelMatrix * vPosition;" \
			"vec3 trasformed_normals = normalize(mat3(uViewMatrix * uModelMatrix) * vNormal);" \
			"vec3 viewer_vector = normalize(-eyeCordinates.xyz);" \

			"light_direction_red = normalize(vec3(uLightPositionRed) - eyeCordinates.xyz);" \
			"float tn_dot_ld_red = max(dot(trasformed_normals,light_direction_red),0.0);" \
			"vec3 ambientRed = uLaRed * uKa;" \
			"vec3 diffuseRed = uLdRed * uKd * tn_dot_ld_red;" \
			"vec3 reflection_vector_red = reflect(-light_direction_red, trasformed_normals);" \
			"vec3 specularRed = uLsRed * uKs * pow(max(dot(reflection_vector_red,viewer_vector),0.0),uMaterialShininess);" \

			"light_direction_green = normalize(vec3(uLightPositionGreen) - eyeCordinates.xyz);" \
			"float tn_dot_ld_green = max(dot(trasformed_normals,light_direction_green),0.0);" \
			"vec3 ambientGreen = uLaGreen * uKa;" \
			"vec3 diffuseGreen = uLdGreen * uKd * tn_dot_ld_green;" \
			"vec3 reflection_vector_green = reflect(-light_direction_green, trasformed_normals);" \
			"vec3 specularGreen = uLsGreen * uKs * pow(max(dot(reflection_vector_green,viewer_vector),0.0),uMaterialShininess);" \

			"light_direction_blue = normalize(vec3(uLightPositionBlue) - eyeCordinates.xyz);" \
			"float tn_dot_ld_blue = max(dot(trasformed_normals,light_direction_blue),0.0);" \
			"vec3 ambientBlue = uLaBlue * uKa;" \
			"vec3 diffuseBlue = uLdBlue * uKd * tn_dot_ld_blue;" \
			"vec3 reflection_vector_blue = reflect(-light_direction_blue, trasformed_normals);" \
			"vec3 specularBlue = uLsBlue * uKs * pow(max(dot(reflection_vector_blue,viewer_vector),0.0),uMaterialShininess);" \

			"phongAdsColor = ambientRed + diffuseRed + specularRed + ambientGreen + diffuseGreen + specularGreen +ambientBlue + diffuseBlue + specularBlue ;" \
		"}" \
		"else if(uFKeyPressed == 1)" \
		"{" \
			"vec4 eyeCordinates = uViewMatrix * uModelMatrix * vPosition;" \
			"transformed_normals = mat3(uViewMatrix * uModelMatrix) * vNormal;" \
			"light_direction_red = vec3(uLightPositionRed) - eyeCordinates.xyz;" \
			"light_direction_green = vec3(uLightPositionGreen) - eyeCordinates.xyz;" \
			"light_direction_blue = vec3(uLightPositionBlue) - eyeCordinates.xyz;" \
			"viewer_vector = -eyeCordinates.xyz;" \
		"}" \
		"else" \
		"{" \
			"phongAdsColor = vec3(1.0,1.0,1.0);"
		"}" \
			"gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vPosition ;" \
		"}";

	//3. provide shader source - glShaderSource()
	glShaderSource(gVertexShaderObject, // Specifies the handle of the shader object whose source code is to be replaced.
		1,								// Specifies the number of elements in the string and length arrays.
		&vertexShaderSourceCode,		// Specifies an array of pointers to strings containing the source code to be loaded into the shader.
		NULL);							// Specifies an array of string lengths.

										//4. compile the shader source
	glCompileShader(gVertexShaderObject);

	// 5. finding error while compilation
	/*
	There might be possibilty in compile time error of our shader source code
	We can fetch these errors using glGetShaderiv API
	We are using this API to get last error from compilation process
	Process to get last error line :
	1. glGetShaderiv gives us the compile status code(GLint), so get it
	2. check it with GL_TRUE(success)/ GL_FALSE(error)
	3. if this is GL_FALSE -
	1. Get log information line length
	2. allocate a memory block of char* size to get this error into
	3. using glGetShaderInfoLog API - get actual error line in above step varibale
	4. prin it and exit from code calling uninitialise()
	*/

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	// get compile status
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE)
	{
		// get info log lengh
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			// allocate memory to hold the error
			szInfoLog = (char *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				// get actual last error line
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Vertex shader compilaton log : %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//******** FRAGMENT SHADER *********
	// create the shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// write the shader source
	const GLchar *fragmentShaderSource =
		"#version 430 core" \
		"\n" \
		"out vec4 FragColor;" \
		"in vec3 phongAdsColor;" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction_red;" \
		"in vec3 light_direction_green;" \
		"in vec3 light_direction_blue;" \
		"in vec3 viewer_vector;" \
		"uniform vec3 uLaRed;" \
		"uniform vec3 uLdRed;" \
		"uniform vec3 uLsRed;" \
		"uniform vec3 uLaGreen;" \
		"uniform vec3 uLdGreen;" \
		"uniform vec3 uLsGreen;" \
		"uniform vec3 uLaBlue;" \
		"uniform vec3 uLdBlue;" \
		"uniform vec3 uLsBlue;" \
		"uniform vec3 uKa;" \
		"uniform vec3 uKd;" \
		"uniform vec3 uKs;" \
		"uniform float uMaterialShininess;" \
		"uniform int uLKeyPressed;" \
		"uniform int uFKeyPressed;" \
		"uniform int uVKeyPressed;" \
		"void main(void)" \
		"{" \
		"if(uFKeyPressed == 1)" \
		"{" \
			"vec3 phongAdsColor;" \
			"vec3 normalisedTransformedVector = normalize(transformed_normals);" \
			"vec3 normalisedViewerVector = normalize(viewer_vector);" \

			"vec3 normalisedLightDirectionRed = normalize(light_direction_red);" \
			"vec3 normalisedLightDirectionGreen = normalize(light_direction_green);" \
			"vec3 normalisedLightDirectionBlue = normalize(light_direction_blue);" \

			"vec3 ambientRed = uLaRed * uKa;" \
			"float tn_dot_ld_red = max(dot(normalisedTransformedVector,normalisedLightDirectionRed),0.0);" \
			"vec3 diffuseRed = uLdRed * uKd * tn_dot_ld_red;" \
			"vec3 reflectionVectorRed = reflect(-normalisedLightDirectionRed,normalisedTransformedVector);" \
			"vec3 specularRed = uLsRed * uKs * pow(max(dot(reflectionVectorRed,normalisedViewerVector),0.0),uMaterialShininess);" \

			"vec3 ambientGreen = uLaGreen * uKa;" \
			"float tn_dot_ld_green = max(dot(normalisedTransformedVector,normalisedLightDirectionGreen),0.0);" \
			"vec3 diffuseGreen = uLdGreen * uKd * tn_dot_ld_green;" \
			"vec3 reflectionVectorGreen = reflect(-normalisedLightDirectionGreen,normalisedTransformedVector);" \
			"vec3 specularGreen = uLsGreen * uKs * pow(max(dot(reflectionVectorGreen,normalisedViewerVector),0.0),uMaterialShininess);" \

			"vec3 ambientBlue = uLaBlue * uKa;" \
			"float tn_dot_ld_blue = max(dot(normalisedTransformedVector,normalisedLightDirectionBlue),0.0);" \
			"vec3 diffuseBlue = uLdBlue * uKd * tn_dot_ld_blue;" \
			"vec3 reflectionVectorBlue = reflect(-normalisedLightDirectionBlue,normalisedTransformedVector);" \
			"vec3 specularBlue = uLsBlue * uKs * pow(max(dot(reflectionVectorBlue,normalisedViewerVector),0.0),uMaterialShininess);" \

			"phongAdsColor = ambientRed + diffuseRed + specularRed + ambientGreen + diffuseGreen + specularGreen +ambientBlue + diffuseBlue + specularBlue;" \
			"FragColor = vec4(phongAdsColor,1.0);" \
		"}" \
		"else if(uVKeyPressed == 1)" \
		"{" \
			"FragColor = vec4(phongAdsColor,1.0);" \
		"}" \
		"else" \
		"{" \
			"vec3 phongAdsColor;" \
			"phongAdsColor =  vec3(1.0,1.0,1.0);" \
			"FragColor = vec4(phongAdsColor,1.0);" \
		"}" \
		"}";

	// provide the source code
	glShaderSource(gFragmentShaderObject, 1, &fragmentShaderSource, NULL);

	// compile the shader source
	glCompileShader(gFragmentShaderObject);

	// 5. finding error while compilation
	/*
	There might be possibilty in compile time error of our shader source code
	We can fetch these errors using glGetShaderiv API
	We are using this API to get last error from compilation process
	Process to get last error line :
	1. glGetShaderiv gives us the compile status code(GLint), so get it
	2. check it with GL_TRUE(success)/ GL_FALSE(error)
	3. if this is GL_FALSE -
	1. Get log information line length
	2. allocate a memory block of char* size to get this error into
	3. using glGetShaderInfoLog API - get actual error line in above step varibale
	4. prin it and exit from code calling uninitialise()
	*/

	// get compiled status
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Fragment shader compilaton log : %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	// ******* SHADER PROGRAM *******
	/*
	We need to attach these compiled source codes to a shader program
	We need to create a shader program for this operation
	Steps
	1. Create the shader program
	2. attach each shader object with the shader program created above
	3. And link this program object for further use
	*/
	//1. Create the shader program
	gShaderProgramObject = glCreateProgram();

	//2. attach each shader object with the shader program created above
	// attaching vertex shader object
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attaching fragment shader object
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// before linking of shader code, we should bind shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, SBM_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, SBM_ATTRIBUTE_NORMAL, "vNormal");

	// LINK the program object
	glLinkProgram(gShaderProgramObject);

	// find out linking time errors
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// after linking - get the uniform , allocate a space to it so that at runtime we can use this space to keep
	// out matrix multiplication into it

	// get all uniform locations
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "uModelMatrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "uViewMatrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "uProjectionMatrix");

	La_red_uniform = glGetUniformLocation(gShaderProgramObject, "uLaRed");
	Ld_red_uniform = glGetUniformLocation(gShaderProgramObject, "uLdRed");
	Ls_red_uniform = glGetUniformLocation(gShaderProgramObject, "uLsRed");
	Light_Position_red_uniform = glGetUniformLocation(gShaderProgramObject, "uLightPositionRed");

	La_green_uniform = glGetUniformLocation(gShaderProgramObject, "uLaGreen");
	Ld_green_uniform = glGetUniformLocation(gShaderProgramObject, "uLdGreen");
	Ls_green_uniform = glGetUniformLocation(gShaderProgramObject, "uLsGreen");
	Light_Position_green_uniform = glGetUniformLocation(gShaderProgramObject, "uLightPositionGreen");

	La_blue_uniform = glGetUniformLocation(gShaderProgramObject, "uLaBlue");
	Ld_blue_uniform = glGetUniformLocation(gShaderProgramObject, "uLdBlue");
	Ls_blue_uniform = glGetUniformLocation(gShaderProgramObject, "uLsBlue");
	Light_Position_blue_uniform = glGetUniformLocation(gShaderProgramObject, "uLightPositionBlue");

	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "uKa");
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "uKd");
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "uKs");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "uMaterialShininess");

	L_Key_PressedUniform = glGetUniformLocation(gShaderProgramObject, "uLKeyPressed");
	F_Key_PressedUniform = glGetUniformLocation(gShaderProgramObject, "uFKeyPressed");
	V_Key_PressedUniform = glGetUniformLocation(gShaderProgramObject, "uVKeyPressed");

	/*
	vertices, colors,shader attribs,vao,vbo initializations
	*/

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	int gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	// code for cube vao and vbo
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// for cube position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SBM_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SBM_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// for cube color
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(SBM_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SBM_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glBindVertexArray(0); // unbind cube vao

	// we are using this linked program object in our display while rendering objects
	// openGL related code
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glShadeModel(GL_SMOOTH);
	// depth settings
	// set up depth buffer
	glClearDepth(1.0f);
	// enable depth test
	glEnable(GL_DEPTH_TEST);
	// depth test to do work
	glDepthFunc(GL_LEQUAL);

	// set orthographic projection matrix to identity
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// start using shader program object
	glUseProgram(gShaderProgramObject);

	if (gbPerVertexLightEnabled == true ) // enable vertex lighting
	{
		glUniform1i(V_Key_PressedUniform, 1);
	}
	else
	{
		glUniform1i(V_Key_PressedUniform, 0);
	}
	if (gbPerFragmentLightEnabled == true) // enable fragment lighting
	{
		glUniform1i(F_Key_PressedUniform, 1);
	}
	else
	{
		glUniform1i(F_Key_PressedUniform, 0);
	}

	if (gbLightEnabled == true)
	{
		glUniform1i(L_Key_PressedUniform, 1);

		glUniform3fv(La_red_uniform, 1, red_lights_ambient);
		glUniform3fv(Ld_red_uniform, 1, red_lights_diffuse);
		glUniform3fv(Ls_red_uniform, 1, red_lights_specular);
		

		glUniform3fv(La_green_uniform, 1, green_lights_ambient);
		glUniform3fv(Ld_green_uniform, 1, green_lights_diffuse);
		glUniform3fv(Ls_green_uniform, 1, green_lights_specular);
		//glUniform4fv(Light_Position_green_uniform, 1, green_lights_position);

		glUniform3fv(La_blue_uniform, 1, blue_lights_ambient);
		glUniform3fv(Ld_blue_uniform, 1, blue_lights_diffuse);
		glUniform3fv(Ls_blue_uniform, 1, blue_lights_specular);
		//glUniform4fv(Light_Position_blue_uniform, 1, blue_lights_position);

		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffused);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		glUniform1i(L_Key_PressedUniform, 0);
	}
	

	// do the drawing ...

	/*
	For CUBE
	*/
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 translationMatrix = mat4::identity();

	// translate of cube
	translationMatrix = translate(0.0f, 0.0f, -1.5f);

	red_lights_position[1] = 3*cos(gRedLightAngle);
	red_lights_position[2] = 3*sin(gRedLightAngle);

	green_lights_position[0] = 3*cos(gGreenLightAngle);
	green_lights_position[1] = 3*sin(gGreenLightAngle);

	blue_lights_position[0] = 3*cos(gBlueLightAngle);
	blue_lights_position[2] = 3*sin(gBlueLightAngle);

	
	modelMatrix = modelMatrix * translationMatrix;
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glUniform4fv(Light_Position_red_uniform, 1, red_lights_position);

	fprintf(gpFile, "lights are %f \n", red_lights_position[1]);


	modelMatrix = mat4::identity();
	modelMatrix = modelMatrix * translationMatrix;
	
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glUniform4fv(Light_Position_green_uniform, 1, green_lights_position);


	modelMatrix = mat4::identity();
	modelMatrix = modelMatrix * translationMatrix;
	
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
	glUniform4fv(Light_Position_blue_uniform, 1, blue_lights_position);


	//bind the vao of square
	glBindVertexArray(gVao_sphere);

	// drawing of square
	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);

	// after drawing stop using shader program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}



void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER |
			SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	// destroy vao

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	// destroy vbo

	if (gVbo_sphere_normal)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	// detach vertex shader
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Closing file !!!");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void update()
{
	gRedLightAngle = gRedLightAngle + 0.001f;
	if (gRedLightAngle >= 360.0f)
	{
		gRedLightAngle = 0.0f;
	}

	gGreenLightAngle = gGreenLightAngle + 0.002f;
	if (gGreenLightAngle >= 360.0f)
	{
		gGreenLightAngle = 0.0f;
	}

	gBlueLightAngle = gBlueLightAngle + 0.003f;
	if (gBlueLightAngle >= 360.0f)
	{
		gBlueLightAngle = 0.0f;
	}
}
