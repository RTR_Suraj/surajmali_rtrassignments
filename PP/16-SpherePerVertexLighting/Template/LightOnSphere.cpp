#include <windows.h>
#include<iostream>

#include<gl/glew.h> //IMPORTANT : This MUST Be Done Before #include For gl\GL.h

#include <gl/GL.h>
#include <gl/GLU.h>
#include "vmath.h"

#include "Sphere.h"

using namespace vmath;

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define PI 3.1415926535898

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"Sphere.lib")

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

// shader related vars
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

FILE *gpFile = NULL;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform;
GLuint view_matrix_uniform;
GLuint projection_matrix_uniform;
GLuint L_Key_PressedUniform;
GLuint La_uniform;
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint Light_Position_uniform;
GLuint material_shininess_uniform;

GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 50.0f;

mat4 gPerspectiveProjectionMatrix;

bool gIsLKeyPressed = false;
bool gbLightEnabled = false;

bool gIsAKeyPressed = false;
bool gbAnimationEnabled = false;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gNumElements;


enum
{
	SBM_ATTRIBUTE_VERTEX = 0,
	SBM_ATTRIBUTE_COLOR,
	SBM_ATTRIBUTE_NORMAL,
	SBM_ATTRIBUTE_TEXTURE0
};


//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display();

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//code
	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL PP code: 3D Rotation of pyramid and cube "),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbAnimationEnabled == true)
				{
					//updateAngle();
				}
				display();
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C:
			if (gIsLKeyPressed  == false)
			{
				gbLightEnabled = true;
				gIsLKeyPressed = true;
			}
			else
			{
				gbLightEnabled = false;
				gIsLKeyPressed = false;	
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE |
			SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);
	void uninitialize();

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code

	fopen_s(&gpFile, "Log.txt", "w");

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// code related to PP ie glewInit
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// get GL_VERSION and GL_SHADING_LANG_VERSION to define them in #version tag in shader source code
	fprintf(gpFile, "GL version: %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//******* VERTEX SHADER *************
	// Steps for using a shader in our code
	/*
	1. Create the shader by providing the argument name of the shader like GL_VERTEX_SHADER,GL_FRAGMENT_SHADER
	2. write source code for the shader as a string with back slash(\) at each line end
	3. provide this shader source
	4. compile the shader source
	5. link the shader source
	6. use it for our drawing
	*/

	//1. Create the shader by providing the argument name of the shader like GL_VERTEX_SHADER,GL_FRAGMENT_SHADER  - glCreateShader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//2. write source code for the shader as a string with back slash(\)
	const GLchar *vertexShaderSourceCode =
		"#version 430 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 uModelMatrix;" \
		"uniform mat4 uViewMatrix;" \
		"uniform mat4 uProjectionMatrix;" \
		"uniform vec3 uLa;" \
		"uniform vec3 uLd;" \
		"uniform vec3 uLs;" \
		"uniform vec3 uKa;" \
		"uniform vec3 uKd;" \
		"uniform vec3 uKs;" \
		"uniform int uLKeyPressed;" \
		"uniform vec4 uLightPosition;" \
		"uniform float uMaterialShininess;" \
		"out vec3 phongAdsColor;" \
		"void main(void)" \
		"{" \
		"if(uLKeyPressed == 1)" \
		"{" \
		"vec4 eyeCordinates = uViewMatrix * uModelMatrix * vPosition;" \
		"vec3 trasformed_normals = normalize(mat3(uViewMatrix * uModelMatrix) * vNormal);" \
		"vec3 light_direction = normalize(vec3(uLightPosition) - eyeCordinates.xyz);" \
		"float tn_dot_ld = max(dot(trasformed_normals,light_direction),0.0);" \
		"vec3 ambient = uLa * uKa;" \
		"vec3 diffuse = uLd * uKd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-light_direction, trasformed_normals);" \
		"vec3 view_vector = normalize(-eyeCordinates.xyz);" \
		"vec3 specular = uLs * uKs * pow(max(dot(reflection_vector,view_vector),0.0),uMaterialShininess);" \
		"phongAdsColor = ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phongAdsColor = vec3(1.0,1.0,1.0);"
		"}" \
		"gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vPosition;" \
		"}";

	//3. provide shader source - glShaderSource()
	glShaderSource(gVertexShaderObject, // Specifies the handle of the shader object whose source code is to be replaced.
		1,								// Specifies the number of elements in the string and length arrays.
		&vertexShaderSourceCode,		// Specifies an array of pointers to strings containing the source code to be loaded into the shader.
		NULL);							// Specifies an array of string lengths.

										//4. compile the shader source
	glCompileShader(gVertexShaderObject);

	// 5. finding error while compilation
	/*
	There might be possibilty in compile time error of our shader source code
	We can fetch these errors using glGetShaderiv API
	We are using this API to get last error from compilation process
	Process to get last error line :
	1. glGetShaderiv gives us the compile status code(GLint), so get it
	2. check it with GL_TRUE(success)/ GL_FALSE(error)
	3. if this is GL_FALSE -
	1. Get log information line length
	2. allocate a memory block of char* size to get this error into
	3. using glGetShaderInfoLog API - get actual error line in above step varibale
	4. prin it and exit from code calling uninitialise()
	*/

	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;

	// get compile status
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE)
	{
		// get info log lengh
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			// allocate memory to hold the error
			szInfoLog = (char *)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				// get actual last error line
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Vertex shader compilaton log : %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	//******** FRAGMENT SHADER *********
	// create the shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// write the shader source
	const GLchar *fragmentShaderSource =
		"#version 430 core" \
		"\n" \
		"out vec4 FragColor;" \
		"in vec3 phongAdsColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(phongAdsColor,1.0);"
		"}";

	// provide the source code
	glShaderSource(gFragmentShaderObject, 1, &fragmentShaderSource, NULL);

	// compile the shader source
	glCompileShader(gFragmentShaderObject);

	// 5. finding error while compilation
	/*
	There might be possibilty in compile time error of our shader source code
	We can fetch these errors using glGetShaderiv API
	We are using this API to get last error from compilation process
	Process to get last error line :
	1. glGetShaderiv gives us the compile status code(GLint), so get it
	2. check it with GL_TRUE(success)/ GL_FALSE(error)
	3. if this is GL_FALSE -
	1. Get log information line length
	2. allocate a memory block of char* size to get this error into
	3. using glGetShaderInfoLog API - get actual error line in above step varibale
	4. prin it and exit from code calling uninitialise()
	*/

	// get compiled status
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);

	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Fragment shader compilaton log : %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}


	// ******* SHADER PROGRAM *******
	/*
	We need to attach these compiled source codes to a shader program
	We need to create a shader program for this operation
	Steps
	1. Create the shader program
	2. attach each shader object with the shader program created above
	3. And link this program object for further use
	*/
	//1. Create the shader program
	gShaderProgramObject = glCreateProgram();

	//2. attach each shader object with the shader program created above
	// attaching vertex shader object
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attaching fragment shader object
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// before linking of shader code, we should bind shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, SBM_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, SBM_ATTRIBUTE_NORMAL, "vNormal");

	// LINK the program object
	glLinkProgram(gShaderProgramObject);

	// find out linking time errors
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);

				fprintf(gpFile, "Shader program link log : %s\n", szInfoLog);

				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// after linking - get the uniform , allocate a space to it so that at runtime we can use this space to keep
	// out matrix multiplication into it

	// get all uniform locations
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "uModelMatrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "uViewMatrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "uProjectionMatrix");
	La_uniform = glGetUniformLocation(gShaderProgramObject, "uLa");
	Ld_uniform = glGetUniformLocation(gShaderProgramObject, "uLd");
	Ls_uniform = glGetUniformLocation(gShaderProgramObject, "uLs");
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "uKa");
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "uKd");
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "uKs");
	L_Key_PressedUniform = glGetUniformLocation(gShaderProgramObject, "uLKeyPressed");
	Light_Position_uniform = glGetUniformLocation(gShaderProgramObject, "uLightPosition");
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "uMaterialShininess");

	/*
	vertices, colors,shader attribs,vao,vbo initializations
	*/

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	int gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	// code for cube vao and vbo
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// for cube position
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(SBM_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SBM_ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// for cube color
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(SBM_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(SBM_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	glBindVertexArray(0); // unbind cube vao

	// we are using this linked program object in our display while rendering objects
	// openGL related code
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glShadeModel(GL_SMOOTH);
	// depth settings
	// set up depth buffer
	glClearDepth(1.0f);
	// enable depth test
	glEnable(GL_DEPTH_TEST);
	// depth test to do work
	glDepthFunc(GL_LEQUAL);

	// set orthographic projection matrix to identity
	gPerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// start using shader program object
	glUseProgram(gShaderProgramObject);

	if (gbLightEnabled == true)
	{
		glUniform1i(L_Key_PressedUniform, 1);

		glUniform3fv(La_uniform, 1, lightAmbient);
		glUniform3fv(Ld_uniform, 1, lightDiffuse);
		glUniform3fv(Ls_uniform, 1, lightSpecular);
		glUniform4fv(Light_Position_uniform, 1, lightPosition);


		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		glUniform1i(L_Key_PressedUniform, 0);
	}

	// do the drawing ...

	/*
	For CUBE
	*/
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 translationMatrix = mat4::identity();

	// translate of cube
	modelMatrix = translate(0.0f, 0.0f, -2.0f);

	//modelMatrix = modelMatrix * translationMatrix;

	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	//bind the vao of square
	glBindVertexArray(gVao_sphere);

	// drawing of square
	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	glBindVertexArray(0);

	// after drawing stop using shader program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}



void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	gPerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER |
			SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}

	// destroy vao

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	// destroy vbo

	if (gVbo_sphere_normal)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteVertexArrays(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	// detach vertex shader
	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	// detach fragment shader
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// unlink shader program
	glUseProgram(0);

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Closing file !!!");
		fclose(gpFile);
		gpFile = NULL;
	}
}
